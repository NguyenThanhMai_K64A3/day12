<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  
  <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
  <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
  <link rel="stylesheet" href="./style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
</head>
<body>
  <?php
    session_start();
    include "database.php";
    include "data.php";
    $username = $_SESSION["username"];
    $gender = $_SESSION["gender"];
    $faculty = $_SESSION["faculty"];
    $address = $_SESSION["address"];
    $birthday = $_SESSION["birthday"];
    $file = $_SESSION["file"];

    if (isset($_POST['submit'])) {
      $new = "INSERT INTO student(name, gender, faculty, birthday, address, avartar) VALUES('$username', '$gender', '$faculty', STR_TO_DATE('$birthday','%d/%m/%Y'), '$address', '$file')";

      if ($connect->query($new)) {
          header("Location: complete_regist.php");
      } 
  }

  ?>
  <form method="post" action="">
    <div class="title">
      <label class="label" for="username">Họ và tên</label><br>
      <p><?php echo $username ?></p> <br>
    </div>

    <div class="title">
      <label class="label" for="gender">Giới tính</label><br>
      <div class="gender" name="gender" id="gender">
      <p><?php echo $genderArr[$gender] ?></p>
      </div>
    </div>

    <div class="title">
      <label class="label" for="faculty">Phân Khoa</label><br>
      <p><?php echo $facultyArr[$faculty] ?></p>
    </div>

    <div class="title">
      <label class="label" for="birthday">Ngày sinh</label><br>
      <p><?php echo $birthday ?></p><br>
    </div>

    <div class="title">
      <label class="label" for="address">Địa chỉ</label><br>
      <p><?php echo $address ?></p><br>
    </div>

    <div class="title">
      <label class="label" for="address">Hình ảnh</label><br>
      <div>
        <img src="<?php echo $file;?>" alt="" width="150" name="file">
      </div>
    </div>

    <button type="submit" name="submit">Xác nhận</button>
  </form>
</body>
</html>
