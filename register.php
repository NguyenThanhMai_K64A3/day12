<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  
  <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
  <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
  <link rel="stylesheet" href="./style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
</head>
<body>
  <?php
    session_start();
    include "database.php";
    include "data.php";
    //Validate
    $usernameErr = $genderErr = $facultyErr = $birthdayErr = $addressErr = $fileErr = "";
    $username = $faculty = $birthday = $address = $file = "";
    
    // print($_POST["gender"]);

    $target_dir = "./upload/";
    $allowedTypes = [
      'image/png' => 'png',
      'image/jpg' => 'jpg',
      'image/jpeg' => 'jpeg',
    ];

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      if (empty($_POST["username"])) {
        $usernameErr = "Hãy nhập tên";
      } else {
        $username = test_input($_POST["username"]);
      }
      
      if (!isset($_POST["gender"])) {
        $genderErr = "Hãy chọn giới tính." ;
        $numErr++;
      } else {
        $gender = test_input($_POST["gender"]);
        $_SESSION['gender'] = $genderArr[$gender];
      }

      if (!isset($_POST["faculty"])) {
        $facultyErr = "Hãy chọn phân khoa." ;
      } else {
        $faculty = test_input($_POST["faculty"]);
        $_SESSION['faculty'] = $facultyArr[$faculty];
      }
      
      if (empty($_POST["birthday"])) {
        $birthdayErr = "Hãy nhập ngày sinh";
        $numErr++;
      } else if (!validateBirthday($_POST["birthday"])) {
        $birthdayErr = "Hãy nhập ngày sinh đúng định dạng";
        $numErr++;
      } 
      else {
        $birthday = test_input($_POST["birthday"]);
      }

      //validate file image
      $filepath = $_FILES['file']['tmp_name'];
      $fileinfo = finfo_open(FILEINFO_MIME_TYPE);
      $filetype = finfo_file($fileinfo, $filepath);
      
      if (!in_array($filetype, array_keys($allowedTypes))) {
        $fileErr = "Hãy chọn file ảnh";
      } 

      // redirect
      if ($usernameErr == "" && $genderErr == "" && $facultyErr == "" && $birthdayErr == "" && $fileErr == "") {
        mkdir($target_dir, "0700");
        // fomat filename
        $date = date('YmdHis');
        $split_filename = explode('.', $_FILES['file']['name']);
        $fomat_filename = $split_filename[0].'_'.$date.'.'.$split_filename[1];
        $target_file = $target_dir . basename($fomat_filename);
        // move file to folder
        move_uploaded_file($filepath, $target_file);
        $_POST["file"] = $target_file;
        $_SESSION = $_POST;
        header("Location: confirm.php");
      }
    }
      
    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
    
    function validateBirthday($birthday){
      $birthdays  = explode('/', $birthday);
      if (count($birthdays) == 3) {
        return checkdate($birthdays[1], $birthdays[0], $birthdays[2]);
      }
      return false;
    }
  ?>
    <form method="post" enctype="multipart/form-data" action=" <?php 
      echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
      <p class="error">
        <?php
          if ($usernameErr != "") echo $usernameErr."<br>";
          if ($genderErr != "") echo $genderErr."<br>";
          if ($facultyErr != "") echo $facultyErr."<br>";
          if ($birthdayErr != "") echo $birthdayErr."<br>";
          if ($fileErr != "") echo $fileErr."<br>";
        ?>
      </p>
    
      <!-- username -->
      <div class="title">
        <label class="label" for="username">Họ và tên<span class="required">*</span></label><br>
        <div><input class="username" type="text" id="username" name="username"><br></div> <br>
      </div>

      <!-- gender -->
      <div class="title">
        <label class="label" for="gender">Giới tính<span class="required">*</span></label><br>
        <div class="gender" id="gender">
          <?php
            foreach($genderArr as $key => $value):
              echo '<div class="gender-option"><input type="radio" name="gender" value='.$key.'><p>'.$value.'</p></div>';
            endforeach;
          ?>
        </div>
      </div>

      <!-- faculty -->
      <div class="title">
        <label class="label" for="faculty">Phân khoa<span class="required">*</span></label><br>
        <select class="faculty" name="faculty" id="faculty">
          <option value=""></option>
          <?php
            foreach($facultyArr as $key => $value):
              echo '<option value="'.$key.'">'.$value.'</option>';
            endforeach;
          ?>
        </select>
      </div>

      <!-- birthday -->
      <div class="title">
        <label class="label" for="birthday">Ngày sinh<span class="required">*</span></label><br>
        <div><input class="username" id="datepicker" name="birthday" placeholder="dd/mm/yyyy"><br></div>
      </div>

      <!-- address -->
      <div class="title">
        <label class="label" for="address">Địa chỉ</label><br>
        <div><input class="username" type="text" id="address" name="address"><br></div>
      </div>

      <!-- image -->
      <div class="title">
        <label class="label" for="address">Hình ảnh</label><br>
        <input class="image" type="file" name="file" accept=".png, .jpg, .jpeg">
      </div>

      <!-- button submit -->
      <button type="submit" name="submit">Đăng ký</button>
    </form>
</body>

<script>
  $( "#datepicker" ).datepicker({
    format: 'dd/mm/yyyy'
  });
</script>
</html>
