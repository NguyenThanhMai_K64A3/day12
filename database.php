<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "student-management";

    $connect = new mysqli($servername, $username, $password, $dbname);

    if ($connect->connect_error) {
        die("Connection failed: " . $connect->connect_error);
    }

    function createTable() {
        global $connect;
        $sql = "
        CREATE TABLE `student` (
            `id` int(11) NOT NULL,
            `name` varchar(250) NOT NULL,
            `gender` int(1) NOT NULL,
            `faculty` char(3) NOT NULL,
            `birthday` datetime NOT NULL,
            `address` varchar(250) DEFAULT NULL,
            `avartar` text DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ";

        if ($connect->query($sql)) {
            echo "Table student create successfully";
        } else {
            echo "Error creating table: " . $connect->error;
        }
        $connect->close();
    }

    function format_date() {
        global $connect;
        $sql = "
            SELECT DATE_FORMAT(birthday, '%d/%m/%Y') FROM student
        ";

        if ($connect->query($sql)) {
            echo "format date successfully";
        } else {
            echo "Error formating date: " . $connect->error;
        }
        
        $connect->close();
    }

    // format_date();

    function addData($username, $gender, $faculty, $birthday, $address, $avartar) {
        global $connect;
        $sql = "INSERT INTO student (name, gender, faculty, birthday, address, avartar)
        VALUES ('$username', '$gender', '$faculty', '$birthday', '$address', '$avartar')";
  
        if ($connect->query($sql)) {
            echo "create successfully";
        } else {
            echo "Error creating " . $connect->error;
        }
        $connect->close();
    }
?>
