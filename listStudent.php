<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  
  <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
  <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
  <link rel="stylesheet" href="./style2.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
</head>
<body>
  <?php
    session_start();
    $khoa = "";
    $keyword = "";
    $pkhoa = array('MAT'=>'Khoa học máy tính', 'KDL'=>'Khoa học vật liệu');
    if (!empty($_SESSION["khoa"])) {
      $khoa = $_SESSION["khoa"];
    }
    if (!empty($_SESSION["keyword"])) {
      $keyword = $_SESSION["keyword"];
    }
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $_SESSION = $_POST;
      $khoa = $_SESSION["khoa"];
      $keyword = $_SESSION["keyword"];
  }

  ?>
    <form id="myForm" method="post" enctype="multipart/form-data" action=" <?php 
      echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
      <!-- khoa -->
      <div class="title">
        <label class="label-list" for="khoa">Khoa</label><br>
        <div id="khoa">
          <select class="username khoa"  id="khoa_id" name="khoa" onkeyup='saveValue(this)'>
            <option value=""></option>
            <?php
              foreach($pkhoa as $key => $value):
                echo '<option value="'.$value.'"';
                if ($khoa == $value) {
                  echo "selected";
                }
                echo '>'.$value.'</option>';
              endforeach;
            ?>
          </select>
        </div>
      </div>

      <!-- keyword -->
      <div class="title">
        <label class="label-list text" for="keyword">Từ khóa</label><br>
        <div><input class="username keyword" type="text" id="keyword" name="keyword" value="<?php echo $keyword;
        ?>"><br></div>
      </div>

      <div class="button title">
        <input type="button" class=" del" id="del" value="Xóa">
        <button type="submit" name="submit" class=" search">Tìm kiếm</button>
      </div>

      <p class="title" >Số sinh viên tìm được: XXX</p>

      <a href="./register.php">
        <input type="button" value="Thêm" class=" add">
      </a>
    </form>
  <!-- List students -->
    <table>
      <tr>
        <th>No</th>
        <th>Tên sinh viên</th>
        <th>Khoa</th>
        <th>Action</th>
      </tr>
      <tr>
        <td>1</td>
        <td>Nguyễn Văn A</td>
        <td>Khoa học máy tính</td>
        <td>
          <input type="button" class="action edit" value="Sửa">
          <input type="button" class="action delete" value="Xóa">
        </td>
      </tr>
      <tr>
      <td>2</td>
        <td>Trần Thị B</td>
        <td>Khoa học máy tính</td>
        <td>
          <input type="button" class="action edit" value="Sửa">
          <input type="button" class="action delete" value="Xóa">
        </td>
      </tr>
      <tr>
      <td>3</td>
        <td>Hoàng Văn C</td>
        <td>Khoa học dữ liệu</td>
        <td>
          <input type="button" class="action edit" value="Sửa">
          <input type="button" class="action delete" value="Xóa">
        </td>
      </tr>
      <tr>
      <td>4</td>
        <td>Đinh Quang D</td>
        <td>Khoa học dữ liệu</td>
        <td>
          <input type="button" class="action edit" value="Sửa">
          <input type="button" class="action delete" value="Xóa">
        </td>
      </tr>
    </table>
</body>

<script>
  $(document).ready(function(){
    $("#del").click(function(){
        localStorage.clear();
        $("#khoa_id").val('');
        $("#keyword").val('');
    });

  });
</script>
</html>
